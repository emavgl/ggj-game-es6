let instance = null

export default class Config {
  constructor () {
    if (!instance) {
      console.log('sono stato creato!')
      instance = this

      this._maxWidth = 1920
      this._maxHeight = 1080
      this._gameWidth = (window.innerWidth > 0 ? window.innerWidth : screen.width)
      this._gameHeight = (window.innerHeight > 0 ? window.innerHeight : screen.height)

      this._aspectRatio = this._gameWidth / this._gameHeight
      if (this._aspectRatio < 1) {
        this._aspectRatio = this._gameHeight / this._maxHeight
      } else {
        this._aspectRatio = this._gameWidth / this._maxWidth
      }
      this._localStorageName = 'Game'
    }
    // to test whether we have singleton or not
    return instance
  }
  getAspectRatio () {
    return this._aspectRatio
  }

  getDimensions () {
    return {'width': this._gameWidth, 'height': this._gameHeight}
  }
}
