/**
 * Storage plugin
 */

let instance = null

let QUOTA_EXCEEDED_ERR = 'QUOTA_EXCEEDED_ERR'

export default class Storage {
  /* Singleton implementation */
  constructor () {
    // to test whether we have singleton or not
    if (!instance) {
      console.log('storage creato!')
      instance = this
    }
    return instance
  }
  /* End singleton interface */

  availability () {
    if (typeof (window.localStorage) === 'undefined') {
      console.log('localStorage not available')
      return null
    }
  };
  get (key) {
    this.availability()
    try {
      return JSON.parse(localStorage.getItem(key))
    } catch (e) {
      return window.localStorage.getItem(key)
    }
  };
  set (key, value) {
    this.availability()
    try {
      window.localStorage.setItem(key, JSON.stringify(value))
    } catch (e) {
      if (e === QUOTA_EXCEEDED_ERR) {
        console.log('localStorage quota exceeded')
      }
    }
  };
  initUnset (key, value) {
    if (this.get(key) === null) {
      this.set(key, value)
    }
  };
  getFloat (key) {
    return parseFloat(this.get(key))
  };
  remove (key) {
    this.availability()
    window.localStorage.removeItem(key)
  };

  clear () {
    this.availability()
    window.localStorage.clear()
  };
}
