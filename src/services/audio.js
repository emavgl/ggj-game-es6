/**
 * Audio
 */

let instance = null

export default class Audio {
  /* Singleton implementation */
  constructor() {
    // to test whether we have singleton or not
    if (!instance) {
      console.log('storage creato!')
      this._sound = [];
      this._audioStatus = true;
      instance = this
    }
    return instance
  }
  /* End singleton interface */

  get audioStatus() {
    return this._audioStatus
  }

  addSound(name, audio, volume = 1) {
    if (!this._sound[name]) {
      this._sound[name] = audio
    }
    audio.volume = volume
    return audio
  }

  enable() {
    this._audioStatus = true
  }
  disable() {
    this._audioStatus = false
  }
  switch() {
    this._audioStatus = !this._audioStatus
  }

  play(sound) {
    if (this._audioStatus) {
      if (this._sound && this._sound[sound]) {
        this._sound[sound].play()
      }
    }
  }
}
