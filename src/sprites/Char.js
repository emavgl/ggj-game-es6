import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Text {
  constructor ({ game, x, y, text }) {
    super(game, x, y, text, {fontSize: '42px', fill: '#FFFFFF'})
    this.anchor.setTo(0.5)
    this.scale.set(new Config().getAspectRatio())
  }

  update(){
    this.body.gravity.x = -200;
  }
}