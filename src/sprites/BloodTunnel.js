import Phaser from 'phaser'
import BaseTunnel from './BaseTunnel'
import Config from '../config'

export default class extends Phaser.Group {
  constructor(game) {
    super(game)
    this.enableBody = true
    this.pastDate = new Date().getTime()
    this.fullFilled = false
    this.itemCount = 0
  }

  update() {
    // move
    this.forEach(function (item) {
      if (item.x < -250) {
        item.x = this.game.world.width + 250
        this.fullFilled = true
      } else {
        item.rotation -= 0.05
      }
    }, this)
    if (!this.fullFilled && this.itemCount < 30) {
      var newDate = new Date().getTime()
      if (newDate > this.pastDate + 300) {
        let baseTunnel = new BaseTunnel({
          game: this.game,
          x: this.game.world.width + 250,
          y: this.game.world.centerY
        })
        this.add(baseTunnel)
        baseTunnel.body.gravity.x = -20
        let ratio = new Config().getAspectRatio() * 0.5
        baseTunnel.body.setSize(208 * ratio, 208 * ratio, 0, 0)
        this.pastDate = newDate
        this.itemCount++
      }
    }
  }
}
