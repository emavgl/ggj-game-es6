import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset }) {
    super(game, x, y, asset);
    this.anchor.setTo(0.5);
    this.scale.set(new Config().getAspectRatio() * 2);
  }

  
  update(){
    this.body.gravity.x = -200;
  }
}
