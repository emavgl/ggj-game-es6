import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Sprite {
  constructor({ game, x, y }) {
    super(game, x, y, 'baseTunnel')
    this.anchor.setTo(0.5)
    let ratio = new Config().getAspectRatio() * 0.5
    this.scale.set(ratio)
  }
}
