import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Sprite {
  constructor({ game, x, y, ref }) {
    super(game, x, y, 'white-globule')
    this.anchor.setTo(0.5)
    this.scale.set(new Config().getAspectRatio() * 0.1)
    this.game.physics.arcade.enable(this)

    this.body.drag.set(70)
    this.body.maxVelocity.set(200)
    this.rotation = this.getAngle(this, ref)
    this.ref = ref
    this.game.physics.arcade.accelerationFromRotation(this.rotation, 100, this.body.acceleration)

  }

  update() {
    this.game.physics.arcade.accelerationFromRotation(this.rotation, 100, this.body.acceleration)
  }
  getAngle(obj1, obj2) {
    // angle in radians
    var angleRadians = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
    return angleRadians
    // angle in degrees
    var angleDeg = (Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x) * 180 / Math.PI);
    return angleDeg;
  }
}
