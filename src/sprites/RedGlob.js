import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Sprite {
  constructor({ game, x, y }) {
    super(game, x, y, 'red-globule')
    this.anchor.setTo(0.5)
    this.scale.set(new Config().getAspectRatio()*0.1)
    this.game.physics.arcade.enable(this)

    this.body.drag.set(70)
    this.body.maxVelocity.set(200)

    this.game.physics.arcade.accelerationFromRotation(this.rotation, -300, this.body.acceleration)

  }

  update() {
    this.game.physics.arcade.accelerationFromRotation(this.rotation, -300, this.body.acceleration)
  }
}
