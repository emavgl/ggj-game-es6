import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Sprite {
    constructor ({ game, x, y, asset }) {
        super(game, x, y, asset);
        this.anchor.setTo(0.5);
        this.scale.set(new Config().getAspectRatio());

        // set arcade physics
        this.game.physics.arcade.enable(this);

        // give the little guy a slight bounce.
        this.body.bounce.y = 0.2;
        this.body.collideWorldBounds = true;

        this.animations.add('moving', [0, 1]);

        // reduce size of the hitbox
        this.body.setSize(110, 150, 50, 25);
    }

    moveUp(){
        this.animations.play('moving', 10, true);
        this.body.velocity.y = -300;
    }

    moveDown(){
        this.animations.play('moving', 10, true);
        this.body.velocity.y = 300;
    }

    stop(){
        //  Reset the players velocity (movement)
        this.animations.stop();
        this.body.velocity.y = 0;
        this.frame = 3;
    }

    collide(body){
        return this.game.physics.arcade.collide(this, platforms);
    }

    overlap(stars, callback, callbackContext){
        return this.game.physics.arcade.overlap(this, stars, callback, null, callbackContext);
    }
}
