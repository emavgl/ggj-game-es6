import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset, velocity}) {
    super(game, x, y, asset)
    this.anchor.setTo(0.5)
    this.scale.set(new Config().getAspectRatio() * 0.1);
    this.game.physics.arcade.enable(this);
    this.body.velocity.x = velocity;
  }
}
