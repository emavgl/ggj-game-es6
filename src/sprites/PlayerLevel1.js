import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Sprite {
    constructor({ game, x, y, asset }) {
        super(game, x, y, asset)
        this.anchor.setTo(0.5)
        this.scale.set(new Config().getAspectRatio())

        // set arcade physics
        this.game.physics.arcade.enable(this)

        this.body.drag.set(70)
        this.body.maxVelocity.set(200)

        this.animations.add('moving', [0, 1])
        // reduce size of the hitbox
        this.body.setSize(110, 150, 50, 25)
        this.cursors = this.game.input.keyboard.createCursorKeys()
    }

    update() {
        if (this.cursors.up.isDown) {
            this.game.physics.arcade.accelerationFromRotation(this.rotation, 300, this.body.acceleration)
            this.animations.play('moving', 10, true);
        } else {
            this.body.acceleration.set(0)
            this.animations.stop();
        }

        if (this.cursors.left.isDown) {
            this.body.angularVelocity = -300
        } else if (this.cursors.right.isDown) {
            this.body.angularVelocity = 300
        } else {
            this.body.angularVelocity = 0
        }

        this.game.world.wrap(this, 16)
    }

}
