import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.Sprite {
    constructor ({ game, x, y, asset }) {
        super(game, x, y, asset);
        this.anchor.setTo(0.5);
        this.scale.set(new Config().getAspectRatio() * 0.2);

        // set arcade physics
        this.game.physics.arcade.enable(this);
        this.body.gravity.y = 0;

        // give the little guy a slight bounce.
        //this.body.bounce.y = 0.2;
        this.body.collideWorldBounds = true;

        this.animations.add('moving');
    }

    update(){
        if (this.body.blocked.up){
            this.body.velocity.y = 300;
        } else if (this.body.blocked.down === true) {
            this.body.velocity.y = -300;
        }

        this.animations.play('moving', 10, true);
    }

    stop(){
        //  Reset the players velocity (movement)
        this.animations.stop();

        this.frame = 3;
    }

    collide(body){
        return this.game.physics.arcade.collide(this, platforms);
    }

    overlap(stars, callback, callbackContext){
        this.game.physics.arcade.overlap(this, stars, callback, null, callbackContext);
    }
}
