/* globals __DEV__ */
import Phaser from 'phaser'
import Player from '../sprites/PlayerLevel1'
import BloodTunnel from '../sprites/BloodTunnel'
import WhiteGlob from '../sprites/WhiteGlob'
import BackgroundBio from '../sprites/backgrounds/BackgroundBio'

export default class extends Phaser.State {
  create() {
    this.time.advancedTiming = true

    //  We're going to be using physics, so enable the Arcade Physics system
    this.physics.startSystem(Phaser.Physics.ARCADE)

    //  A simple background for our game
    this.bg = new BackgroundBio(this.game)
    this.game.add.existing(this.bg)
    this.tunnel = new BloodTunnel(this.game)
    this.game.add.existing(this.tunnel)

    // The player and its settings
    this.player = new Player({
      game: this.game,
      x: this.world.centerX,
      y: this.world.centerY,
      asset: 'alien'
    })
    this.game.add.existing(this.player)

    this.globs = this.game.add.group();
    this.globs.enableBody = true;
    //  The score
    this.score = 0
    this._time = 15
    let glob;
    this.currentTimer = this.game.time.create()
    this.currentTimer.loop(Phaser.Timer.SECOND, function () {
      this._time--
      glob = new WhiteGlob({
        game: this.game,
        x: (this._time % 2 === 0) ? (Math.random() > 0.5 ? 0 : this.world.width) : Math.random() * this.world.width,
        y: (this._time % 2 !== 0) ? (Math.random() > 0.5 ? 0 : this.world.height) : Math.random() * this.world.heigth,
        ref: this.player
      })
      this.globs.add(glob)
      if (this._time > -1) {
        this.textTime.setText('Time left: ' + this._time)
      } else {
        this.currentTimer.stop()
        this.state.start('TowardLevel2')
      }
    }, this)
    this.currentTimer.start()

    var fontTime = { font: "32px Arial", fill: "#FFF" }
    this.textTime = this.add.text(this.world.width - 30, this.world.height - 20, 'Time left: ' + this._time, fontTime);
    this.textTime.anchor.set(1, 1)
  }

  init() {

  }

  update() {
    if (__DEV__) {
      this.game.debug.spriteInfo(this.player, 32, 32)
      this.game.debug.text(this.game.time.fps, this.world.width - 100, 14, '#00ff00')
    }

    this.player.overlap(this.tunnel, this.collideTunnel, this)
    this.bg.filterUpdate()
    if (this.tunnel.y > -100) {
      this.tunnel.y -= 1
    } else {
      this.tunnel.y = this.world.height + 100
    }
    if (!this.overlap(this.player, this.tunnel, this.collideTunnel, this)) {
      this.overlap(this.player, this.globs, this.collideTunnel, this)
    }

  }

  overlap(obj, target, callback, callbackContext) {
    return this.game.physics.arcade.overlap(obj, target, callback, null, callbackContext);
  }

  collideTunnel(player, star) {
    console.log('gameover')
    this.playerIsDead(player)
  }

  playerIsDead(player) {
    this.currentTimer.stop();
    player.kill();

    // Create texts
    this.gameoverText = this.game.add.text(this.world.centerX, this.world.centerY, "GAME OVER", {
      fontSize: '82px',
      fill: '#FFFFFF'
    });

    this.restartText = this.game.add.text(this.world.centerX, this.world.centerY + 100, "Restarting in 5 seconds", {
      fontSize: '82px',
      fill: '#FFFFFF'
    });

    // centers
    this.restartText.anchor.set(0.5);
    this.gameoverText.anchor.set(0.5);

    this.game.time.events.add(Phaser.Timer.SECOND * 5, this.restartGame, this);
  }

  restartGame() {
    this.state.start('GameLevel1');
  }

}
