import Phaser from 'phaser'
import Config from '../config'

export default class extends Phaser.State {

    create() {
        this.config = new Config();
        this.width = this.config.getDimensions()['width'];
        this.height = this.config.getDimensions()['height'];

        this.space = this.game.add.sprite(0, 0, 'space-intro');
        this.space.scale.set(3 * this.config.getAspectRatio());

        // Earth
        this.earth = this.game.add.sprite(this.width * 0.10, this.height * 0.65,
            'earth-intro');
        this.earth.scale.set(0.05 * this.config.getAspectRatio());

        // Ship
        this.ship = this.game.add.sprite(this.width * 0.80, this.height * 0.15, 'ship');
        this.ship.animations.add('animate');
        this.ship.animations.play('animate', 30, true);

        // Move earth
        var tweenEarth = this.game.add.tween(this.earth);
        tweenEarth.to({ width: 500, height: 500, y: this.height * 0.30 }, 5000, 'Linear', true, 0);

        // Move ship
        var tweenShip = this.game.add.tween(this.ship);
        tweenShip.to({ width: 0, height: 0, x: this.width * 0.25, y: this.height * 0.5 }, 5000, 'Linear', true, 0);

        /* var words = [ 'sopra', 'la', 'terra', 'una', 'navicella', 'sorvola', 
            'minacciosa', 'e', 'punta', 'dritto', 'alla', 'nostra', 'città.' ];
        this.text = "Spazio..."; */
        var text = "Spazio...\nSopra la terra una navicella sorvola minacciosa e punta dritto alla nostra città"
        this.bmpText = this.game.add.bitmapText(this.width / 2, this.height * 0.69, "gem", text, 34);
        this.bmpText.maxWidth = 600;

        let skipButton = this.add.button(this.width * 0.80, this.height * 0.83, 'skip-button',
            this.nextScene, this);




        //  Write out 200 random words
        //this.game.time.events.repeat(100, 200, this.addText(words), this);
    }

    /*addText(words){
        var word = words.splice(0, 1)[0];
        this.text += word;
        this.bmpText.text = word;
        
    }*/

    // TODO: scale line 25
    // TODO: animazione del testo
    render() {
        //this.earth.width = this.width * 0.05
    }

    preload() {
        this.game.load.bitmapFont('gem', './assets/fonts/gem.png', './assets/fonts/gem.xml');
    }

    nextScene() {
        this.state.start('Landing')
    }

}