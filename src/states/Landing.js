import Phaser from 'phaser'
import Config from '../config'
import Audio from '../services/audio'

export default class extends Phaser.State {

  preload() {
    // TODO SPOSTARE CARICMANEOT FONT IN CONFIG
    this.game.load.bitmapFont('gem', './assets/fonts/gem.png', './assets/fonts/gem.xml');
  }

  create() {
    this.config = new Config();
    this.width = this.config.getDimensions()['width'];
    this.height = this.config.getDimensions()['height'];

    this.space = this.game.add.sprite(0, 0, 'landing-sky');
    this.space.scale.set(4 * this.config.getAspectRatio());

    // First part of the text
    var text = "L'astronave a contatto con la ionosfera precipita e sembra avere un'avaria.\n" + 
      "Il pilota Duddyts è destinato a fallire l'invasione ancora prima di iniziare?";
    this.bmpText = this.game.add.bitmapText(this.width * 0.6, this.height * 0.69, "gem", text, 34);
    this.bmpText.maxWidth = 600;
    var tweenText = this.game.add.tween(this.bmpText);
    tweenText.to({y: this.height * 0.05}, 8000, 'Linear', true, 0);

    // Ship
    this.ship = this.game.add.sprite(this.width * 0.43, this.height * 0.05, 'ship');
    this.ship.scale.set(0.5 * this.config.getAspectRatio());
    this.ship.animations.add('animate');
    this.ship.animations.play('animate', 30, true);

    var tweenShip = this.game.add.tween(this.ship);
    tweenShip.to({y: this.height, width: 0, height: 0}, 10000, 'Linear', true, 0);

    for (var i = 0; i < 40; i++) {
      var cloud = this.game.add.sprite(Math.floor((Math.random() * this.width) + 1), this.height + 1000 * i, 'landing-cloud1');
      var tweenCloud = this.game.add.tween(cloud);
      tweenCloud.to({ y: -300 }, 500 + 200 * i, 'Linear', true, 0);
     
    }

    tweenCloud.onComplete.add(this.showCup, this)
    let skipButton = this.add.button(this.width * 0.80, this.height * 0.83, 'skip-button', this.nextScene, this);
  }

  fade() {
    this.game.camera.fade(0x000000, 5000);
    let audio = new Audio();
    audio.play('glu-glu')
    this.game.camera.onFadeComplete.add(this.nextScene, this)
  }

  nextScene(){
    this.state.start('CupTransition');
  }
  
  showCup(){
    this.cup = this.game.add.sprite(this.game.world.centerX * 0.4, this.height, 'landing-cup');
    var tweenCup = this.game.add.tween(this.cup);
    tweenCup.to({y: this.height * 0.05}, 1000, 'Linear', true, 0);
    tweenCup.onComplete.add(this.fade, this)
  }
}