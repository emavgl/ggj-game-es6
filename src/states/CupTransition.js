import Phaser from 'phaser'
import Config from '../config'
import Audio from '../services/audio'

export default class extends Phaser.State {
    preload() {
        // TODO SPOSTARE CARICMANEOT FONT IN CONFIG
        this.game.load.bitmapFont('gem', './assets/fonts/gem.png', './assets/fonts/gem.xml');
    }

    create() {
        this.config = new Config();
        this.width = this.config.getDimensions()['width'];
        this.height = this.config.getDimensions()['height'];
        
        this.space = this.game.add.sprite(0, 0, 'black-screen');
        // First part of the text
        var text = "E' arrivato il momento per Duddyts per mettersi in azione, ma è debole e deve recuperare le energie per " +
            "prendere il controllo del corpo dell'ospite.\nFatti forza Duddyts!";
        this.bmpText = this.game.add.bitmapText(this.width * 0.4, this.height * 0.5, "gem", text, 32);
        this.bmpText.maxWidth = 800;

        let skipButton = this.add.button(this.width * 0.80, this.height * 0.83, 'skip-button', this.nextScene, this);
    }
    nextScene(){
        this.state.start('Menu');
    }
}