import Phaser from 'phaser'
import Config from '../config'
import Player from '../sprites/Player'
import { toAspectRatio } from '../utils';


export default class extends Phaser.State {
	create() {
		this.config = new Config();
		this.widthX = this.config.getDimensions()['width'];
		this.heightY = this.config.getDimensions()['height'];
		
		this.infected = this.game.add.sprite(this.widthX *0.3, this.heightY * 0.6, 'infected');
		this.infected.anchor.set(0.5, 0.5);
		this.infected.scale.set(this.config.getAspectRatio());

		this.notInfected = this.game.add.sprite(this.widthX *0.5, this.heightY * 0.65, 'not-infected');
		this.notInfected.anchor.set(0.5, 0.5);
		this.notInfected.scale.set(this.config.getAspectRatio());

		this.player = new Player({
			game: this.game,
			x: this.infected.x -200,
			y: this.infected.y - 180,
			asset: 'alien'
		});
		
		this.game.add.existing(this.player);
		this.player.scale.set(toAspectRatio(0.20));	

		var tweenPlayer = this.game.add.tween(this.player);
		tweenPlayer.to({ x: this.notInfected.x +130 }, 3000, 'Linear', true, 0);
		this.player.animations.play('moving', 10, true);

		tweenPlayer.onComplete.add(this.showText, this);

	}

	showText() {
		this.charText = this.game.add.text(16, 40, "Duddyts! Duddyts! Duddyts!\nDuddyts ha compiuto il suo primo\npasso verso la conquista del mondo\nTrasmetti il tuo amore Duddyts!", {
		fontSize: '32px',
		fill: '#000'
		})

		this.game.time.events.add(5000, this.startLevel, this);
	}

	startLevel(){
		this.state.start('Credits');
	}
}