import Phaser from 'phaser'
import { centerGameObjects } from '../utils'
import Config from '../config'
import Audio from '../services/audio'

export default class extends Phaser.State {
  init () {
	this.stage.backgroundColor = '#EDEEC9';
  }

  preload() {
    // preload sprite
    let startButton = this.add.button(this.game.world.centerX, this.game.world.centerY, 'start-button', this.startGame, this);
    startButton.anchor.set(0.5, 0.5)
    startButton.scale.set(new Config().getAspectRatio() * 0.1)
    let audio = new Audio();
    audio.play('soundMusic');
  }

  create () {
  	  this.config = new Config();
      this.width = this.config.getDimensions()['width'];
      this.height = this.config.getDimensions()['height'];
	  let skipButton = this.add.button(this.width * 0.80, this.height * 0.83, 'skip-button', this.credits, this);

	  var text = "Credits"
	  this.game.add.text(this.width * 0.87, this.height * 0.855, text, { fontSize: "32px", fill: "#000"});

	  this.title = this.game.add.sprite(this.game.world.centerX + 50, this.height * 0.3, 'game-title');
	  this.title.anchor.set(0.5, 0.5)
	  this.title.scale.set(new Config().getAspectRatio()*0.5);

	  this.ggj = this.game.add.sprite(this.width * 0.20, this.height * 0.83, 'ggjTrento');
	  this.ggj.anchor.set(0.5, 0.5)
	  this.ggj.scale.set(new Config().getAspectRatio()*0.5);
  }

  startGame() {
    let audio = new Audio();
    audio.play('click')
    this.state.start('GameLevel1');
  }

  credits () {
  	  this.state.start('Credits');
  }
}
