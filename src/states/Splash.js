import Phaser from 'phaser'
import { centerGameObjects } from '../utils'
import Audio from '../services/audio'

export default class extends Phaser.State {
  init () {
    this.resources = {
      'image': [
        ['start-button', './assets/images/start-button.png'],
        ['sky', './assets/images/sky.png'],
        ['star', './assets/images/star.png'],
        ['red-laser', './assets/images/red-laser.png'],
        ['green-laser', './assets/images/green-laser.png'],
        ['space-intro', './assets/images/space-intro.png'],
        ['earth-intro', './assets/images/earth-intro.png'],
        ['skip-button', './assets/images/skip-button.png'],
        ['landing-sky', './assets/images/landing-sky.png'],
        ['landing-cloud1', './assets/images/landing-cloud1.png'],
        ['black-screen', './assets/images/black.png'],
        ['diamond', './assets/images/diamond.png'],
        ['game-title', './assets/images/game-title.png'],
        ['toward-Level2', './assets/images/toward-Level2.png'],
        ['pink-bar', './assets/images/pink-bar.png'],
        ['brain', './assets/images/brain_image.png'],
        ['landing-cup', './assets/images/landing-cup.png'],
        ['baseTunnel', './assets/images/baseTunnel.png'],
        ['landing-cup', './assets/images/landing-cup.png'],
        ['infected', './assets/images/infected.png'],
        ['not-infected', './assets/images/not-infected.png'],
        ['ggjTrento', './assets/images/ggjTrento.png']
      ],
      'spritesheet': [
        ['dude', 'assets/images/dude.png', 32, 48],
        ['ship', 'assets/images/ship.png', 502, 395]
      ],
      'atlas': [
        ['alien', './assets/images/alien.png', './assets/images/alien.json'],
        ['red-globule', './assets/images/red-globule.png', './assets/images/red-globule.json'],
        ['white-globule', './assets/images/GlobW.png', './assets/images/red-globule.json']
      ],
      'audio': [
        ['audio-click', ['assets/sfx/audio-button.m4a', 'assets/sfx/audio-button.mp3', 'assets/sfx/audio-button.ogg']],
        ['audio-theme', ['assets/sfx/music-bitsnbites-liver.m4a', 'assets/sfx/music-bitsnbites-liver.mp3', 'assets/sfx/music-bitsnbites-liver.ogg']],
        ['glu-glu', 'assets/sfx/gluglu.mp3']
      ]
    }
  }

  preload () {
    // preload sprite
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.load.setPreloadSprite(this.loaderBar)
    //
    // load your assets
    //
    this._preloadResources()
  }

  _preloadResources () {
    var pack = this.resources
    for (var method in pack) {
      pack[method].forEach(function (args) {
        var loader = this.load[method]
        loader && loader.apply(this.load, args)
      }, this)
    }
  }
  
  create() {
    let audio = new Audio()
    audio.addSound('click', this.game.add.audio('audio-click'))
    audio.addSound('soundMusic', this.game.add.audio('audio-theme', 1, true), 0.5)
    audio.addSound('glu-glu', this.game.add.audio('glu-glu'))
    this.state.start('Intro')
  }
}
