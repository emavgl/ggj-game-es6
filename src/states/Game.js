/* globals __DEV__ */
import Phaser from 'phaser'
import Player from '../sprites/Player'
import Enemy from '../sprites/Enemy'
import Star from '../sprites/Star'
import Config from '../config'
import Laser from '../sprites/Laser'
import Storage from '../services/storage'
import { toAspectRatio, shuffleArray, charToKeyCode } from '../utils';
import Char from '../sprites/Char';
import BackgroundDigital from '../sprites/backgrounds/BackgroundDigital'

export default class extends Phaser.State {
  create() {
    this.time.advancedTiming = true
    this.pastDate = new Date().getTime()
    this.message = 'password';
    this.collectedChar = []
    this.collectedAmmo = 0
    this.remainChars = shuffleArray(this.message.split(''))
    this.messageLength = this.message.length;

    //  We're going to be using physics, so enable the Arcade Physics system
    this.physics.startSystem(Phaser.Physics.ARCADE)

    //  A simple background for our game
    this.bg = new BackgroundDigital(this.game)
    this.game.add.existing(this.bg)

    // The player and its settings
    this.player = new Player({
      game: this.game,
      x: 250 * new Config().getAspectRatio(),
      y: this.world.centerY,
      asset: 'alien'
    })

    this.game.add.existing(this.player)

    // Ad enemy
    this.enemy = new Enemy({
      game: this.game,
      x: this.world.width - 200,
      y: 0,
      asset: 'red-globule'
    });

    this.game.add.existing(this.enemy);

    //  Groups
    this.stars = this.game.add.group();
    this.stars.enableBody = true;

    this.playerLasers = this.game.add.group();
    this.playerLasers.enableBody = true;

    this.enemyLasers = this.game.add.group();
    this.enemyLasers.enableBody = true;

    this.chars = this.game.add.group();
    this.chars.enableBody = true;

    //  The score
    this.score = 0
    this.ammoText = this.game.add.text(16, 16, 'ammo: ', {
      fontSize: '32px',
      fill: '#FFFFFF'
    })

    this.charText = this.game.add.text(16, 50, 'collected: ', {
      fontSize: '32px',
      fill: '#FFFFFF'
    })

    this.remainCharsText = this.game.add.text(16, this.world.height - 100, 'chars out: ', {
      fontSize: '32px',
      fill: '#FFFFFF'
    })

    //  Our controls.
    let that = this
    this.input.keyboard.onDownCallback = function (e) {
      that.controls(e, that)
    }

    this.input.keyboard.onUpCallback = function (e) {
      that.player.stop()
    }

/*    this.currentTimer = this.game.time.create()
    this.currentTimer.loop(Phaser.Timer.SECOND, function () {
      this.stars.rotation -= 0.1
      this.player.rotation += 0.33
    }, this)
    this.currentTimer.start()
    */
  }

  init() {

  }

  controls(e, instance) {
    if (e.keyCode == 38) {
      //  Move to the up
      instance.player.moveUp()
    } else if (e.keyCode == 40) {
      //  Move to the down
      instance.player.moveDown();
    } else if (e.keyCode == 32) {
      this.newPlayerLaserOut(instance);
    } else if (e.keyCode == 27 && instance.remainChars.length == 0){
      this.passwordManager(instance);
    } else {
      this.checkOtherKeysPressed(instance, e);
    }
  }

  render() {
    if (__DEV__) {
      this.game.debug.spriteInfo(this.enemy, 32, 32);
      this.game.debug.text(this.game.time.fps, this.world.width - 100, 14, "#00ff00");
      this.game.debug.body(this.player);
    }

    this.newAction();

    this.updateStats();

    this.checkForCollision();

    this.bg.filterUpdate();
  }

  passwordManager(instance){
    let attempts = 3;
    let promptMessage = "Insert the password. Use the collected chars as a hint\n";
    promptMessage += "Collected chars: " + instance.collectedChar.join('-') + "\n";
    promptMessage += "Remember, the password is " + instance.messageLength.toString() + " chars long\n";
   
    let win = false;
    while (attempts > 0){
      let attemptMessage = "You have " +  attempts + " attempts remain";
      let answer = prompt(promptMessage + attemptMessage, "Keyword");
      if (answer == instance.message){
        // You got it
        instance.showWinMessageAndRestart();
        win = true;
        break;
      } else {
        attempts -= 1;
      }
    }

    // Game over
    if (!win){
      instance.playerIsDead(instance.player, null);
    }
  }

  showWinMessageAndRestart(){
    this.player.kill();

    // Create texts
    this.youWinText = this.game.add.text(this.world.centerX, this.world.centerY, "YOU WIN!", {
      fontSize: '82px',
      fill: '#FFFFFF'
    });

    this.restartText = this.game.add.text(this.world.centerX, this.world.centerY + 100, "Wait 5 seconds", {
      fontSize: '82px',
      fill: '#FFFFFF'
    });

    // centers
    this.restartText.anchor.set(0.5);
    this.youWinText.anchor.set(0.5);

    this.game.time.events.add(Phaser.Timer.SECOND * 5, this.goToEnding, this);
  }

  goToEnding(){
    this.state.start("EndingVideo");
  }

  updateStats(){
    this.charText.text = 'collected: ' + this.collectedChar.join(' ');
    this.ammoText.text = 'ammo: ' + this.collectedAmmo;
    let charsOutCount = this.messageLength - this.remainChars.length;
    this.remainCharsText.text =  'chars out: ' + charsOutCount.toString() +  '/' + this.messageLength.toString();

    if (charsOutCount == this.messageLength) {
      this.remainCharsText.text = "ALL OUT - PRESS ESC TO INSERT THE PASSWORD";
    }
  }

  checkOtherKeysPressed(instance, e){
    if (instance.charCollideKey){
      let collideChar = instance.charCollideKey.text;

      if (charToKeyCode(collideChar) == e.keyCode){
        instance.collectedChar.push(collideChar);
        console.log(instance.collectedChar.toString());
        
        instance.charCollideKey.kill();
        
        // delete global entry
        instance.charCollideKey = null
      }
    }
  }

  checkForCollision(){
    //  Check for collision with stars
    this.player.overlap(this.stars, this.collectStar, this);

    // Call the method charCollision if a char collide
    // otherwhise reset the global variable
    // remove global variable so that
    // the player can't collect the char
    if (!this.player.overlap(this.chars, this.charCollision, this)){
      this.charCollideKey = null;
    }

    // Check if an enemy's laser hit the player
    // if yes, game over
    this.player.overlap(this.enemyLasers, this.playerIsDead, this);

    this.enemy.overlap(this.playerLasers, this.enemyIsDead, this);
  }

  newAction(){
    var newDate = new Date().getTime()
    if (newDate > this.pastDate + Math.floor(Math.random() * 5000) + 1000) {
  
      let magicNumber = Math.floor(Math.random() * 10) + 1;
      
      if (magicNumber == 4){
        // 20% create a char
        this.newCharOut();
      }
      
      if (magicNumber == 3 && !this.enemy.alive) {
        // 10% spawn a new enemy
          this.enemy.reset(this.world.width - 200, 0);
      }
      
      if (magicNumber > 6 && this.enemy.alive) {
        // 40% laser
        this.newEnemyLaserOut();
      }

      if (magicNumber > 7){
        // 30%
        this.newStarOut();
      }

      // Update timer
      this.pastDate = newDate;
    }
  }

  newCharOut(){
    let charToLaunch = this.remainChars.pop();
    let newChar = new Char({
      game: this.game,
      x: this.world.width,
      y: Math.floor(Math.random() * this.world.height) + 50,
      text: charToLaunch
    })

    this.chars.add(newChar);
  }

  newStarOut(){
    let star = new Star({
      game: this.game,
      x: this.world.width,
      y: Math.floor(Math.random() * this.world.height) + 50,
      asset: 'star'
    });

    this.stars.add(star);
  }

  newEnemyLaserOut(){
    let enemyLaser = new Laser({
      game: this.game,
      x: this.enemy.x,
      y: this.enemy.y,
      asset: 'red-laser',
      velocity: -400
    });

    this.enemyLasers.add(enemyLaser);
  }

  newPlayerLaserOut(instance){
    if (instance.collectedAmmo > 0){
      let laser = new Laser({
        game: instance.game,
        x: instance.player.x,
        y: instance.player.y,
        asset: 'green-laser',
        velocity: 500
      });
      instance.playerLasers.add(laser);
      instance.collectedAmmo -= 1;
    }
  }

  enemyIsDead(enemy, laser){
    enemy.kill();
  }

  playerIsDead(player, collideLaser){
    player.kill();

    // Create texts
    this.gameoverText = this.game.add.text(this.world.centerX, this.world.centerY, "GAME OVER", {
      fontSize: '82px',
      fill: '#FFFFFF'
    });

    this.restartText = this.game.add.text(this.world.centerX, this.world.centerY + 100, "Restarting in 5 seconds", {
      fontSize: '82px',
      fill: '#FFFFFF'
    });

    // centers
    this.restartText.anchor.set(0.5);
    this.gameoverText.anchor.set(0.5);

    this.game.time.events.add(Phaser.Timer.SECOND * 5, this.restartGame, this);
  }

  restartGame(){
    this.state.start('Game');
  }

  charCollision(player, collideChar){
    console.log("collide with", collideChar.text);
    this.charCollideKey = collideChar;
  }

  collectStar(player, star) {
    // Removes the star from the screen
    this.collectedAmmo += 1
    star.kill()
  }
}
