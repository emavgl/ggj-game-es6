import Phaser from 'phaser'
import Config from '../config'
import Player from '../sprites/Player'
import { toAspectRatio } from '../utils';


export default class extends Phaser.State {
	create() {
		this.config = new Config();
		this.widthX = this.config.getDimensions()['width'];
		this.heightY = this.config.getDimensions()['height'];

		this.TL2 = this.game.add.sprite(0, 0, 'toward-Level2');
		this.TL2.scale.set(5 * this.config.getAspectRatio());

		this.pinkBar = this.game.add.sprite(0, this.heightY* 0.9, 'pink-bar');
		this.pinkBar.scale.set(5 * this.config.getAspectRatio());

		this.brain = this.game.add.sprite(this.widthX *0.75, this.heightY * 0.53, 'brain');
		this.brain.anchor.set(0.5, 0.5);
		this.brain.scale.set(this.config.getAspectRatio());

		this.player = new Player({
			game: this.game,
			x: 0,
			y: this.world.height - 105,
			asset: 'alien'
		});

		this.game.add.existing(this.player);
		this.player.scale.set(toAspectRatio(0.5));	

		var tweenPlayer = this.game.add.tween(this.player);
		tweenPlayer.to({ x: 350 }, 3000, 'Linear', true, 0);
		this.player.animations.play('moving', 10, true);

		tweenPlayer.onComplete.add(this.showText, this);

	}

	showText() {
		this.charText = this.game.add.text(16, 50, "Duddits e' riuscito ad evolversi\ned a raggiungere il cervello dell'ospite,\ne' ora di prenderne il controllo.\nTrova le parole Duddits!", {
		fontSize: '32px',
		fill: '#000'
		})

		this.game.time.events.add(3000, this.goToBrain, this);

	}

	goToBrain(){
			var tweenPlayer = this.game.add.tween(this.player);
			tweenPlayer.to({ x: this.widthX *0.75 }, 3000, 'Linear', true, 0);
			this.player.animations.play('moving', 10, true);
			tweenPlayer.onComplete.add(this.startLevel, this);	
	}

	startLevel(){
		this.state.start('Game');
	}
}