import Phaser from 'phaser'
import Config from '../config'
import WebFont from 'webfontloader'

export default class extends Phaser.State {
	init () {
		this.stage.backgroundColor = '#000';
  }
	
	create() {
		this.config = new Config();
        this.width = this.config.getDimensions()['width'];
        this.height = this.config.getDimensions()['height'];

		var credit = "Team Developer\n\nLevel Design & Coding:\n  Adriano Costa (Level 1)\n  Emanuele Viglianisi (Level 2)\n\nGraphics & Story:\n  Carlo Costa\n\nVideo Makers:\n  Alessandro Torresani\n  Andrea Giarrusso\n\n\nTechnologies:\n  Phaser\n  OpenGL\n";
		this.bmpCredit = this.game.add.bitmapText(this.width *0.30, this.height * 0.9, "gem", credit, 34);
		this.bmpCredit.maxWidth = 600;

		var tweenCredit = this.game.add.tween(this.bmpCredit);
        tweenCredit.to({ y: -700 }, 8000, 'Linear', true, 0);

		tweenCredit.onComplete.add(this.nextScene, this)
	}

	nextScene() {
		if (!this.done) {
		this.state.start('Menu')
    }
	}
}