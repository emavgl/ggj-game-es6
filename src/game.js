import 'pixi'
import 'p2'
import Phaser from 'phaser'

import BootState from './states/Boot'
import SplashState from './states/Splash'
import IntroState from './states/Intro'
import LandingStage from './states/Landing'
import CupTransition from './states/CupTransition'
import GameLevel1 from './states/GameLevel1'
import GameLevel2 from './states/Game'
import MenuState from './states/Menu'
import CreditsState from './states/Credits'
import TowardLevel2State from './states/TowardLevel2'
import EndingState from './states/EndingVideo'
import Config from './config'

let instance = null

export default class Game extends Phaser.Game {
  constructor () {
    let config = new Config()
    let dimensions = config.getDimensions()
    super(dimensions.width, dimensions.height, Phaser.AUTO, 'content', null)

    if (!instance) {
      instance = this
      this.state.add('Boot', BootState, false)
      this.state.add('Splash', SplashState, false)
      this.state.add('Intro', IntroState, false)
      this.state.add('Landing', LandingStage, false)
      this.state.add('CupTransition', CupTransition, false)
      this.state.add('Game', GameLevel2, false)
      this.state.add('GameLevel1', GameLevel1, false)
      this.state.add('Menu', MenuState, false)
      this.state.add('Credits', CreditsState, false)
      this.state.add('TowardLevel2', TowardLevel2State, false)
      this.state.add('EndingVideo', EndingState, false)
    }

    // to test whether we have singleton or not
    return instance
  }

  StartGame () {
    this.state.start('Boot')
  }
}
